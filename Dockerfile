FROM node
RUN apt-get update && apt-get install -y vim nano
RUN mkdir /testbox

COPY package.json /testbox
WORKDIR /testbox

RUN yarn install
COPY . /testbox
RUN yarn test
RUN yarn build

CMD yarn start


EXPOSE 3000
